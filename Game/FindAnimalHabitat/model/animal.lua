return {
    {
        image_sheet = 'Game/assets/img/sprite_chicken.png',
        sprite_settings = {
            height = 150,
            numFrames = 14,
            width = 150
        },
        sequence_settings = {
            count = 14,
            loopDirection = 'forward',
            name = 'animal_run',
            start = 1,
            time = 500
        },
        id = 1,
        habitat_id = 6
    }, {
        image_sheet = 'Game/assets/img/sprite_cow.png',
        sprite_settings = {
            height = 150,
            numFrames = 4,
            width = 150
        }, 
        sequence_settings = {
            count = 4,
            loopDirection = 'forward',
            name = 'animal_run',
            start = 1,
            time = 500
        },        
        id = 2,
        habitat_id = 6
    }, {
        image_sheet = 'Game/assets/img/sprite_penguin.png',
        sprite_settings = {
            height = 150,
            numFrames = 12,
            width = 130
        },
        sequence_settings = {
            count = 12,
            loopDirection = 'forward',
            name = 'animal_run',
            start = 1,
            time = 500
        },        
        id = 3,
        habitat_id = 5
    }, {
        image_sheet = 'Game/assets/img/sprite_running_dog.png',
        sprite_settings = {
            height = 150,
            numFrames = 6,
            width = 150
        },
        sequence_settings = {
            count = 6,
            loopDirection = 'forward',
            name = 'animal_run',
            start = 1,
            time = 500
        },        
        id = 4,
        habitat_id = 3
    }
}