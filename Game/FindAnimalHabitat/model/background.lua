return {
    {
        imageRect = 'Game/assets/img/bg_desert.png',
        id = 1,
        name = 'desert'
    }, {
        imageRect = 'Game/assets/img/bg_forest.png',
        id = 2,
        name = 'forest'
    }, {
        imageRect = 'Game/assets/img/bg_house.png',
        id = 3,
        name = 'house'
    }, {
        imageRect = 'Game/assets/img/bg_living_room.png',
        id = 4,
        name = 'living_room'
    }, {
        imageRect = 'Game/assets/img/bg_northpole.png',
        id = 5,
        name = 'northpole'
    }, {
        imageRect = 'Game/assets/img/bg_rural.png',
        id = 6,
        name = 'rural'
    }, {
        imageRect = 'Game/assets/img/bg_sea.png',
        id = 7,
        name = 'sea'
    }, {
        imageRect = 'Game/assets/img/bg_sky.png',
        id = 8,
        name = 'sky'
    }
}