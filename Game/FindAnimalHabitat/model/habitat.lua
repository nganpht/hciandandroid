return {
    {
        icon ='Game/assets/img/ic_habitat_desert.png',
        id = 1,
        name = 'desert'
    }, {
        icon ='Game/assets/img/ic_habitat_forest.png',
        id = 2,
        name = 'forest'
    }, {
        icon ='Game/assets/img/ic_habitat_house.png',
        id = 3,
        name = 'house'
    }, {
        icon ='Game/assets/img/ic_habitat_living_room.png',
        id = 4,
        name = 'living_room'
    }, {
        icon ='Game/assets/img/ic_habitat_northpole.png',
        id = 5,
        name = 'northpole'
    }, {
        icon ='Game/assets/img/ic_habitat_rural.png',
        id = 6,
        name = 'rural'
    }, {
        icon ='Game/assets/img/ic_habitat_sea.png',
        id = 7,
        name = 'sea'
    }, {
        icon ='Game/assets/img/ic_habitat_sky.png',
        id = 8,
        name = 'sky'
    }
}