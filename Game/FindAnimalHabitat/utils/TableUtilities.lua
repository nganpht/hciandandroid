-- Check an element whether it exists in a specific table or not
function contain(table, element) 
    for index, value  in ipairs(table) do
        if element == value then
            return true
        end
    end

    return false
end
