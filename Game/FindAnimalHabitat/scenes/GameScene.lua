local physics = require('physics')
physics.start()
physics.setGravity(0.0, 0.0)

local composer = require('composer')
local scene = composer.newScene()

-- Game/assets
local animal_sprite = require('Game.FindAnimalHabitat.model.animal')
local background_img = require('Game.FindAnimalHabitat.model.background')
local habitat_ic = require('Game.FindAnimalHabitat.model.habitat')

-- Scene View
local this

-- Scene Components
local animal
local background
local background_id
local habitatAnserId
local habitatQuestionId = {}
local habitatQuestion = {}

-- Random number
local random

-- Utils
local utils = require('Game.FindAnimalHabitat.utils.TableUtilities')

-- Function called to init a scene before it shows
function scene:create(event)
    this = self.view

    background_id = event.params.background_id

    -- Background component init
    background = display.newImageRect(background_img[background_id].imageRect, 300, 500)
    background.x = display.contentCenterX
    background.y = display.contentCenterY
    background.alpha = 0.5    

    -- Boundary
    local bottomWall = display.newRect(0, display.contentHeight, display.contentWidth, 1)
    local leftWall = display.newRect(0, 0, 1, display.contentHeight)
    local rightWall = display.newRect (display.contentWidth, 0, 1, display.contentHeight)
    local topWall = display.newRect (0, 0, display.contentWidth, 1)

    physics.addBody(bottomWall, 'static', { bounce = 0.1 })
    physics.addBody (leftWall, 'static', { bounce = 0.1} )
    physics.addBody (rightWall, 'static', { bounce = 0.1} )
    physics.addBody (topWall, 'static', { bounce = 0.1} )

    -- Call a random for an animal
    random = math.random(#animal_sprite) -- random a number from 1 to max element in animal_sprite table        

    -- Animal sprite init    
    animal = display.newSprite(
        graphics.newImageSheet(
            animal_sprite[random].image_sheet, 
            animal_sprite[random].sprite_settings
        ), 
        animal_sprite[random].sequence_settings
    )
    animal.x = display.contentCenterX
    animal.y = display.contentCenterY            

    -- For checking collsion purpose
    animal.id = animal_sprite[random].habitat_id    

    animal:scale(0.5, 0.5)

    -- Add physics for collision
    physics.addBody(animal, { bounce = 0.0, friction = 0.3 })
    
    animal.collision = onCollision
    animal:addEventListener('collision')

    -- Run the sprite animation
    animal:setSequence('animal_run')
    animal:play()

    -- Store the answer ID
    habitatAnserId = habitat_ic[animal_sprite[random].habitat_id].id        

    -- Init question list ID
    for i = 1, 4, 1 do
        local temp = math.random(1, #habitat_ic) -- temp for random number from 1 to max element of habitat_ic
        
        -- Check for dupplication ID
        while (contain(habitatQuestionId, temp)) do
            temp = math.random(1, #habitat_ic)            
        end                

        table.insert(habitatQuestionId, i, temp) -- insert the randomized ID
    end
    
    -- Check if the correct answer is included
    if not contain(habitatQuestionId, habitatAnserId) then
        local temp = math.random(1, 4) -- temp for random position for correct answer

        table.insert(habitatQuestionId,temp, habitatAnserId) -- replace it
    end

    -- Init the questions list    
    for i = 1, 4, 1 do
        -- Show the question component to UI
        habitatQuestion[i] = display.newImageRect(habitat_ic[habitatQuestionId[i]].icon, 125, 125)

        -- For checking collision purpose
        habitatQuestion[i].id = habitatQuestionId[i]
    end
            
    -- Re-arrange the questions position
        -- Top-left position
    habitatQuestion[1].x = 87.5
    habitatQuestion[1].y = 87.5    
        -- Top-right position
    habitatQuestion[2].x = display.contentWidth - 87.5
    habitatQuestion[2].y = 87.5    
        -- Bottom-left position
    habitatQuestion[3].x = 87.5
    habitatQuestion[3].y = display.contentHeight - 87.5    
        -- Bottom-right position
    habitatQuestion[4].x = display.contentWidth - 87.5
    habitatQuestion[4].y = display.contentHeight - 87.5        
    
    -- Add components  to Scene View
    this:insert(background)
    this:insert(animal)    

    -- Add physics and view
    for i = 1, 4 , 1 do
        physics.addBody(habitatQuestion[i], { bounce = 0.0, radius = 10 })

        this:insert(habitatQuestion[i])
    end


    -- Interval calls for accelerometer
    system.setAccelerometerInterval(60)

    -- Event listener for accelerometer 
    Runtime:addEventListener('accelerometer', onAccelerate)
end

-- Function called when a scene is hide (before go to another scene)
function scene:hide(event)
    if event.phase == 'did' then
        Runtime:removeEventListener('accelerometer', onAccelerate)

        composer.removeScene('Game.FindAnimalHabitat.scenes.GameScene')
    end
end

function onAccelerate(event)
    animal.x = animal.x + (animal.x * event.xGravity * 0.01) 
    animal.y = animal.y + (animal.y * event.yGravity * 0.01 * -1) 
end

function onCollision(self, event) 
    if event.phase == 'began' then
        local params

        if self.id == event.other.id then
            params = {
                background_id = event.other.id,
                status = 'true'
            }
        else 
            params = {
                background_id = background_id,
                status = 'false'
            }
        end
        
        composer.gotoScene('Game.FindAnimalHabitat.scenes.ResultScene', { 
            effect = 'crossFade', 
            params = params,
            time = 500
        })
    end
end

scene:addEventListener('create', scene)
scene:addEventListener('hide', scene)
scene:addEventListener('show', scene)

return scene