local composer = require('composer')
local scene = composer.newScene()

-- Game/assets
local background_img = require('Game.FindAnimalHabitat.model.background')

-- Scene View
local this

-- Scene Components
local background
local background_id
local ic_home
local ic_next
local ic_status

-- Animate the object to rotate up and down rapidly
local function rotate(object)
    transition.to(object, {        
        y = object.y + 5,
        time = 350,
        onComplete = function ()
            transition.to(object, {                 
                y = object.y - 5,
                time = 350,
                onComplete = function ()
                    rotate(object);
                end
            })
        end
    })
end

-- Function called to init a scene before it shows
function scene:create(event)
    this = self.view
    
    background_id = event.params.background_id -- set the background based on the previous answer

    -- Background component init
    background = display.newImageRect(background_img[background_id].imageRect, 300, 500)
    background.x = display.contentCenterX
    background.y = display.contentCenterY

    -- Icon status component init (creates based on params 'status' passed in GameScene)
    local statusSheet
    local statusOptions
    if event.params.status == 'true' then
        statusSheet = graphics.newImageSheet('Game/assets/img/sprite_dog_clapping.png', {
            height = 288,
            numFrames = 10,
            width = 288
        })        
        statusOptions = {
            count = 10,
            loopDirection = 'forward',
            name = 'status_sequence',
            start = 1,
            time = 500
        }
    else
        statusSheet = graphics.newImageSheet('Game/assets/img/sprite_dove_ugly.png', {
            height = 288,
            numFrames = 2,
            width = 288
        })  
        statusOptions = {
            count = 2,
            loopDirection = 'forward',
            name = 'status_sequence',
            start = 1,
            time = 500
        }  
    end

    ic_status = display.newSprite(this, statusSheet, statusOptions)
    ic_status.x = display.contentCenterX
    ic_status.y = display.contentCenterY

    ic_status:scale(0.5, 0.5)

    -- Run the sprite animation
    ic_status:setSequence('status_sequence')
    ic_status:play()

    -- Icon home component init
    ic_home = display.newImageRect('Game/assets/img/ic_home.png', 50, 50)
    ic_home.x = 75
    ic_home.y = display.contentHeight - 75     

    -- Icon next component init
    ic_next = display.newImageRect('Game/assets/img/ic_next.png', 50, 50)
    ic_next.x = display.contentWidth - 75
    ic_next.y = display.contentHeight - 75

    -- Add to Scene View
    this:insert(background)
    this:insert(ic_status)
    this:insert(ic_home)
    this:insert(ic_next)    

    rotate(ic_home)
    rotate(ic_next)
end

-- Function called when a scene is hide (before go to another scene)
function scene:hide(event)
    if event.phase == 'did' then
        composer.removeScene('Game.FindAnimalHabitat.scenes.ResultScene')
    end
end

-- Function called when a scene is created and shown to UI
function scene:show(event) 
    if event.phase == 'did' then
        ic_home:addEventListener('touch', function(event) 
            if event.phase == 'ended' then
                composer.gotoScene('Game.FindAnimalHabitat.scenes.StartScene', { effect = 'crossFade', time = 500 })
            end
        end)

        ic_next:addEventListener('touch', function(event)
            if event.phase == 'ended' then
                composer.gotoScene('Game.FindAnimalHabitat.scenes.GameScene', { 
                    effect = 'crossFade', 
                    params = { background_id = background_id },
                    time = 500 
                })
            end
        end)
    end
end

scene:addEventListener('create', scene)
scene:addEventListener('hide', hide)
scene:addEventListener('show', scene)

return scene