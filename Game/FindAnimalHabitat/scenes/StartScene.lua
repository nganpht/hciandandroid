local composer = require('composer')
local scene = composer.newScene()

-- Game/assets
local background_img = require('Game.FindAnimalHabitat.model.background')

-- Scene view
local this

-- Scene components
local background
local logo
local ic_touch

-- Random number
local background_id

-- Function called to init a scene before it shows
function scene:create(event)
    this = self.view

    -- Random an integer number for background ID
    background_id = math.random(#background_img) -- random a number from 1 to max element in background_img table

    -- Background init
    background = display.newImageRect(background_img[background_id].imageRect, 300, 500)
    background.x = display.contentCenterX
    background.y = display.contentCenterY     

    -- Logo init
    logo = display.newImageRect('Game/assets/img/logo.png', 300, 270)
    logo:scale(0.7, 0.7)
    logo.x = display.contentCenterX
    logo.y = display.contentCenterY * 0.8

    -- Icon Touch sprite init
    local iconSheet = graphics.newImageSheet('Game/assets/img/ic_touch.png', {
        height = 450,
        numFrames = 12,
        width = 296
    })
    ic_touch = display.newSprite(this, iconSheet, {
        count = 12,
        loopDirection = 'forward',
        name = 'icon_touch',
        start = 1,
        time = 250
    })
    ic_touch.x = display.contentCenterX
    ic_touch.y = display.contentHeight * 0.7

    ic_touch:scale(0.15, 0.15)

    ic_touch:setSequence('icon_touch')
    ic_touch:play()
        
    -- Add to Scene View
    this:insert(background)
    this:insert(logo)
    this:insert(ic_touch)
end

-- Function called when a scene is hide (before go to another scene)
function scene:hide(event)
    if event.phase == 'did' then    
        composer.removeScene('Game.FindAnimalHabitat.scenes.StartScene')
    end
end

-- Function called when a scene is created and shown to UI
function scene:show(event)     
    if event.phase == 'did' then        
        background:addEventListener('touch', function(event) 
            if event.phase == 'ended' then 
                composer.gotoScene('Game.FindAnimalHabitat.scenes.GameScene', { 
                    effect = 'crossFade', 
                    params = { background_id =  background_id },
                    time = 500
                })
            end
        end)
    end
end

scene:addEventListener('create', scene)
scene:addEventListener('hide', scene)
scene:addEventListener('show', scene)

return scene