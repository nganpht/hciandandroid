module(..., package.seeall)
local content = require("EnglishGame.content")
function newButtonLetter(letter)
    local btnBackground = display.newImage("assets/img/EnglishGame/purple.png")
    local letterGraphic = display.newImage("assets/img/EnglishGame/letters/"..letter..".png")
    btnBackground.width = 80
    btnBackground.height = 80
    letterGraphic.width = 80
    letterGraphic.height = 80
    btnBackground.rotation = 90
    letterGraphic.rotation = 90
    local letterButton = {}
	letterButton.graphics = display.newGroup()
	letterButton.graphics:insert(btnBackground)
	letterButton.graphics:insert(letterGraphic)
	letterButton.letter = letter
	return letterButton	
end

function getWrongLetter(letter)
    local alphabet = {"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"}
    
    local randomPos = math.random(#alphabet)
    while (alphabet[randomPos] == letter) do
        randomPos = math.random(#alphabet)
    end
    return alphabet[randomPos]
end