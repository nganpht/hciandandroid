local composer = require("composer")
local scene = composer.newScene()
local widget = require( "widget" )
local content = require("EnglishGame.content")
local factory = require("EnglishGame.factory")
local sceneGroup
local wordGraphic
local oldScence, newScence
local currentPosQuestion
local letterButtons
local correctButton, wrongButton
local letter, wrongLetter
local myAnimation
local backButton
local wrongSign
audio.reserveChannels(8)
audio.setVolume(1, {channel = 8})
local loadVoice
local filePath = ""
local fileSound = ""
local sqlite3 = require( "sqlite3" )
local path = system.pathForFile( "data.db", system.DocumentsDirectory )
local db = sqlite3.open( path ) 
local countTime = composer.getVariable("countTime")
local wordButtonGroup

local function rotateObject90d(object)
	object:rotate(90)
end
local function rotateImg90d(img)
	img.rotation = 90;
end
local sequenceData = {
    { name="seq2", start=1, count=8, time=1000, loopCount=1000 }
}
local function loadSound()
    audio.stop(8)
    audio.play(loadVoice, {channel = 8})
end

local function loadSheet()
    myAnimation = display.newSprite(content[currentPosQuestion].sheet, sequenceData )
    myAnimation.rotation = 90
    myAnimation.x = display.contentCenterX * 1.35
    myAnimation.y = display.contentHeight/2
    myAnimation:setSequence( "seq2" )
    myAnimation:play()
    myAnimation:addEventListener("tap", loadSound)
    audio.stop(8)
    audio.play(loadVoice, {channel = 8})
end
local function loadNewQuestion()
    currentPosQuestion = math.random(#content)
    for row in db:nrows("SELECT * FROM tblEnglishGame where id = " .. content[currentPosQuestion].id  ) do
        filePath = row.content;
        fileSound = row.sound;
    end
    loadVoice = audio.loadSound(fileSound)
    loadSheet()
    letter = content[currentPosQuestion].word:sub(1,1)
    wrongLetter = factory.getWrongLetter(letter)
end
local wrongSound = audio.loadSound("assets/audio/EnglishGame/wrongsound.wav")
local function loadWord()
    -- transition.to(wrongSign, { time=500, alpha=0})
    -- wordGraphic = display.newGroup()
    local word = content[currentPosQuestion].word
    wordGraphic = display.newGroup()
    correctButton:removeSelf()
    for i=1, string.len(word) do
        local char = word:sub(i, i)
        local btnTmpChar = factory.newButtonLetter(char, 80).graphics
        wordGraphic:insert(btnTmpChar)
        btnTmpChar.alpha = 0
        transition.to(btnTmpChar, { time=1000, alpha=1, x=display.contentCenterX * 0.5, y=(btnTmpChar.width * 0.72) * i})
    end
    local englishVoice = audio.loadSound(filePath)
    audio.stop()
    audio.play(englishVoice, {channel = 8})
    transition.to(backButton, { time=500, alpha=0})
end

function reloadGame()
    wordGraphic:removeSelf()
    composer.removeScene("EnglishGame.game")
    composer.gotoScene("EnglishGame.game", {time = "1000", effect = "fade"})
end
function onCorrect()
    loadWord()
    timer.performWithDelay(3000, reloadGame)
end
function loadCorrecWord()
    
end
function onLetterTouch(event)
	local t = event.target
	if "ended" == event.phase then
		if t == correctButton then
			onCorrect()
		else
			onIncorrect(t)
		end
	end
end
local function loadNewBtnWord(letter, btnChar, startPos)
    -- local width, strLen
    -- if (countTime < 2) then
    --     strLen = string.len(letter)
    --     width = 35
    -- elseif (countTime >= 2 and countTime < 10) then
    --     strLen = 2
    --     width = 50
    -- elseif (countTime >= 10) then
    --     strLen = 1
    --     width = 60
    -- end
    -- for i=1, strLen  do
    --     local char = letter:sub(i, i)
    --     btnChar = factory.newButtonLetter(char, width).graphics
    --     wordButtonGroup:insert(btnChar)
    --     btnChar.x=display.contentCenterX * 0.5
    --     btnChar.y=(btnChar.width * 0.72) * i + display.contentCenterY*startPos
    --     if (letter == wrongLetter) then
    --         btnChar:addEventListener("touch", onIncorrect)
    --     else
    --         btnChar:addEventListener("touch", onCorrect)
    --     end
    -- end
    
end

local function pushButton(leftButton, rightButton)
    leftButton.x = display.contentCenterX * 0.5
    leftButton.y = display.contentCenterY * 0.8
    rightButton.x = display.contentCenterX * 0.5
    rightButton.y = display.contentCenterY * 1.2
end

local function loadNewLayout()
    correctButton = widget.newButton({
        width = 100,
        height = 60,
        defaultFile = "assets/img/EnglishGame/btnAnswer.png",
        overFile = "assets/img/EnglishGame/btnAnswer_pressed.png",
        onEvent = onLetterTouch
    })
    correctButton.x = display.contentCenterX * 0.6
    correctButton.y = display.contentCenterY
    rotateImg90d(correctButton)

    local randomButtonPos = math.random( 1,2)

end
local function gotoMainMenu()
    audio.stop()
    composer.gotoScene( "EnglishGame.menu3", { time=1000, effect="crossFade" } )
    composer.removeScene("EnglishGame.game", { time=1000, effect="crossFade" })
end

local function handleBackToMenuEvent(event) 
	if("ended" == event.phase) then
        display.remove(loadAnimal);
        -- wordButtonGroup:removeSelf()
		gotoMainMenu();
	end
end
-- create() 
function scene:create(event)
	sceneGroup = self.view
	local background = display.newImageRect(sceneGroup, "assets/img/EnglishGame/backgroundJul.png", 300, 500 )
    background.x = display.contentCenterX
    background.y = display.contentCenterY	
    -- wordButtonGroup = display.newGroup()
    loadNewQuestion()
    loadNewLayout()
    sceneGroup:insert(correctButton)
    -- sceneGroup:insert(wrongButton)
    sceneGroup:insert(myAnimation)
    backButton = widget.newButton({
		width = 50,
		height = 50,
		defaultFile = "assets/img/EnglishGame/btnBack.png",
		overFile = "assets/img/EnglishGame/btnBack_pressed.png",
		onEvent = handleBackToMenuEvent
	})
	backButton.x = display.contentWidth - 60;
    backButton.y = 40;
    transition.to(backButton, { time=500, alpha=1})
    sceneGroup:insert(backButton);
    rotateImg90d(backButton)
    wrongSign = display.newImageRect(sceneGroup, "assets/img/EnglishGame/wrongSign.png", 130, 130)
    wrongSign.x = display.contentCenterX
    wrongSign.y = display.contentCenterY
    wrongSign.alpha = 0
    
end
-- show() 
function scene:show(event)
	local sceneGroup  = self.view;
    local phase = event.phase;
    
    if(phase == "will") then
            
    elseif(phase == "did") then
        
	end
end
-- hide() 
function scene:hide( event )

	local sceneGroup = self.view
	local phase = event.phase
    if ( phase == "will" ) then

	elseif ( phase == "did" ) then
	end
end
-- destroy() 
function scene:destroy( event )
	local sceneGroup = self.view
end

scene:addEventListener("create", scene);
scene:addEventListener("show", scene);
scene:addEventListener("hide", scene);
scene:addEventListener("destroy", scene);

return scene;
