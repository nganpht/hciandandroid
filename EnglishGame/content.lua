return {
    {
        sheet = graphics.newImageSheet( "assets/img/EnglishGame/cat2.png", { width=150, height=150, numFrames=10, sheetContentWidth=1800, sheetContentHeight=150 }),
        id = "1",
        word = "cat"
    },
    {
        sheet = graphics.newImageSheet( "assets/img/EnglishGame/chicken1.png", { width=150, height=150, numFrames=10, sheetContentWidth=1800, sheetContentHeight=150 }),
        id = "2",
        word = "chicken"
    },
    {
        sheet = graphics.newImageSheet( "assets/img/EnglishGame/dog1.png", { width=150, height=150, numFrames=14, sheetContentWidth=2400, sheetContentHeight=150 }),
        id = "3",
        word = "dog"
    },
    {
        sheet = graphics.newImageSheet( "assets/img/EnglishGame/frog1.png", { width=160, height=160, numFrames=8, sheetContentWidth=1440, sheetContentHeight=160 }),
        id = "4",
        word = "frog"
    },
    {
        sheet = graphics.newImageSheet( "assets/img/EnglishGame/pig1.png", { width=144, height=144, numFrames=10, sheetContentWidth=3600, sheetContentHeight=144 }),
        id = "5",
        word = "pig"
    },
    {
        sheet = graphics.newImageSheet( "assets/img/EnglishGame/chicken2.png", { width=150, height=150, numFrames=14, sheetContentWidth=2400, sheetContentHeight=150 }),
        id = "6",
        word = "chicken"
    }
    
}