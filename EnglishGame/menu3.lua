local composer = require( "composer" )
local scene = composer.newScene()
local widget = require( "widget" )

local loopGame;

local bgSoundPlay;

local bgSound = audio.loadStream( "assets/audio/EnglishGame/canon.wav" );
audio.setVolume(1, {channel = 7});
bgSoundPlay = audio.play( bgSound ,{ channel=7, loops=-1, fadein=2000 })
	
composer.setVariable("bgSound", bgSound);

local function gotoGame()
	audio.stop();
	composer.gotoScene( "EnglishGame.game", { time=800, effect="crossFade" } )
end

local function gotoMainMenu()
	audio.stop();
	bgSoundPlay = nil;	
	local backgroundSound = composer.getVariable("backgroundSound")
	audio.play(backgroundSound);
	
	composer.gotoScene( "menu-game", { time=800, effect="crossFade" } )
end

local function rotateObject90d(object)
	object:rotate(90)
end
local function rotateImg90d(img)
	img.rotation = 90;
end
local function handleNextButtonEvent(event) 
	if("ended" == event.phase) then
		gotoGame();
	end
end

local function handleBackToMenuEvent(event) 
	if("ended" == event.phase) then
		gotoMainMenu();
	end
end

local function moveObject(object) 
	transition.to(object, {
		y = object.y - 5,
		time = 500,
		onComplete = function ()
			transition.to(object, {
				y = object.y + 5;
				time = 500;
				onComplete = function ()
					moveObject(object);
				end
			})
		end
	} )
end
local function rotateObject(object) 
	transition.to(object, {
		rotation = 85,
		time = 500,
		onComplete = function ()
			transition.to(object, {
				rotation = 95,
				time = 500;
				onComplete = function ()
					rotateObject(object);
				end
			})
		end
	} )
end

local function rotateObj(object)
    transition.to(object, {
        --rotation = 0.5,
        x = object.x + 2,
        time = 700,
        onComplete = function ()
            transition.to(object, { 
                --rotation = -0.5,
                x = object.x - 2,
                time = 700,
                onComplete = function ()
                    rotateObj(object);
                end
            })
        end
    })
end

-- create() 
function scene:create( event )
	
	local sceneGroup = self.view
	-- -- Code here runs when the scene is first created but has not yet appeared on screen
	
	local background = display.newImageRect( sceneGroup, "assets/img/EnglishGame/backgroundJul.png", 300, 500 )
	background.x = display.contentCenterX
	background.y = display.contentCenterY

	local logo = display.newImageRect(sceneGroup, "assets/img/EnglishGame/AnimalSoundLogo.png", 190, 160);
	logo.x = display.contentCenterX
	logo.y = display.contentCenterY * 0.7;
	rotateImg90d(logo);
	rotateObj(logo);
	local backButton = widget.newButton({
		width = 50,
		height = 50,
		defaultFile = "assets/img/EnglishGame/btnBack.png",
		overFile = "assets/img/EnglishGame/btnBack_pressed.png",
		onEvent = handleBackToMenuEvent
	})
	backButton.x = display.contentWidth - 60;
	backButton.y = 40;
	sceneGroup:insert(backButton);
	
	rotateObject90d(backButton)
	moveObject(backButton)
	local btnPlayEnglishGame = widget.newButton({
		width = 120,
		height = 120,
		defaultFile = "assets/img/EnglishGame/btnGame.png",
		overFile = "assets/img/EnglishGame/btnGame_pressed.png",
		onEvent = handleNextButtonEvent
	})
	btnPlayEnglishGame.x = display.contentCenterX * 1.35;
	btnPlayEnglishGame.y = display.contentCenterY * 1.5
	sceneGroup:insert(btnPlayEnglishGame)
	rotateObject90d(btnPlayEnglishGame)
	moveObject(btnPlayEnglishGame);
end
-- show() 
function scene:show( event ) 
	local sceneGroup  = self.view;
	local phase = event.phase

	if ( phase == "will" ) then
		-- Code here runs when the scene is still off screen (but is about to come on screen)
		
	elseif ( phase == "did" ) then
		-- Code here runs when the scene is entirely on screen
		-- Start the music!
		local isChannel7Playing = audio.isChannelPlaying(7);
		if isChannel7Playing then
		elseif(isChannel7Playing == false) then
			local bgSound = audio.loadStream( "assets/audio/EnglishGame/canon.wav" );
			audio.setVolume(1, {channel=7});
			bgSoundPlay = audio.play( bgSound ,{ channel=7, loops=-1, fadein=2000 })
				
			composer.setVariable("bgSound", bgSound);
		end
	end
end
-- hide() 
function scene:hide( event )
	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
		-- Code here runs when the scene is on screen (but is about to go off screen)
		
	elseif ( phase == "did" ) then
		-- Code here runs immediately after the scene goes entirely off screen
		
	end
end
-- destroy () 
function scene:destroy(event) 
	local sceneGroup = self.view
end

scene:addEventListener("create", scene);
scene:addEventListener("show", scene);
scene:addEventListener("hide", scene);
scene:addEventListener("destroy", scene);

return scene;
